﻿using UnityEngine;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;

namespace Assets.Resources.Code.States
{
    class SetupState : IStateBase
    {
        private StateManager manager;   // manager game class type

        /// <summary>
        /// Create the state
        /// </summary>
        /// <returns></returns>
        public SetupState(StateManager managerRef)    // constructor
        {
            manager = managerRef;   // store a reference to StateManager class

            // Load Scene00
            if (Application.loadedLevelName != "Scene00")
            {
                Application.LoadLevel("Scene00");
            }
        }

        /// <summary>
        /// Update is called by stateManager once per frame when state active
        /// </summary>
        /// <returns></returns>
        public void StateUpdate()
        {
        }

        /// <summary>
        /// called by stateManager onGUI once per frame when state active
        /// </summary>
        /// <returns></returns>
        public void ShowIt()
        {
            // draw the texture store in instructionStateSplash variable that is the size of the screen
            GUI.DrawTexture(new Rect(Screen.width / 2 - 256 / 2, Screen.height / 2 - 128 / 2, 256, 128), manager.gameDataRef.setupStateSplash, ScaleMode.StretchToFill);

            GUI.Box(new Rect(Screen.width / 2 - 160 / 2, Screen.height / 2, 160, 120), "Setup state");

            // if button pressed or I keys
            if (GUI.Button(new Rect(10, 10, 250, 60), "Press Here or I to Instruction") || Input.GetKeyUp(KeyCode.I))
            {
                // switch the state to Setup state
                manager.SwitchState(new InstructionState(manager));
            }

            // if button pressed or P keys
            if (GUI.Button(new Rect(Screen.width - 250 - 5, 10, 250, 60), "Press Here or Any P to Play") || Input.GetKeyUp(KeyCode.P))
            {
                // set permanence timer for the force thereshold to a value
                manager.gameDataRef.forceManager.RemainingTimeSetted = 5;

                // set lowr limt for the force
                manager.gameDataRef.forceManager.ForceLowerLimit = 5;

                // switch the state to Setup state
                manager.SwitchState(new PlayState(manager));
            }

            Debug.Log("In SetupState");
        }
    }
}